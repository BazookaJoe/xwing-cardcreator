<?php
$locale = App::getLocale();
?>

@extends('app')

@section('title', 'X-Wing 2.0 Upgrade/Pilot Card Creator')

@section('content')
    <div class="row">
        <div class="col s12 m6 center">
            <h2>{{ ucfirst( __('pilot cards')) }}</h2>
            <a href="/{{ $locale }}/pilots">
                <img src="/img/card-example/pilot-0.png" id="pilotImage">
            </a>
        </div>

        <div class="col s12 m6 center">
            <div>
                <h2>{{ ucfirst( __('upgrade cards')) }}</h2>
                <a href="/{{ $locale }}/upgrades">
                    <img src="/img/card-example/upgrade-0.png" id="upgradeImage">
                </a>
            </div>
        </div>
        <div class="col s12 center mini">
            <h2>{{ ucfirst( __('gallery')) }}</h2>
            @foreach ($gallery_cards as $gallery_card)
                <a href="/{{ $locale }}/gallery">
                    <img src="{{ $gallery_card }}" id="upgradeImage" class="mini">
                </a>
            @endforeach
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            // var imageNumber = Math.floor(Math.random() * (6 - 1 + 1) + 1);
            // $('img#upgradeImage').attr('src', './img/card-example-upgrade-' + imageNumber + '.png');
        });
    </script>
@endsection
