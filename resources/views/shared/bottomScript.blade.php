<script>
    $(document).ready(function () {
        M.AutoInit();
    });

    function displayLoadingStart() {
        $('div#outputImageContainer div.btn').addClass('disabled');
        $('#cardLoader').css('display', 'inline-block');
    }

    function displayLoadingEndSuccess(){
        $('div#outputImageContainer div.btn').removeClass('disabled');
        $('#cardLoader').css('display', 'none');
    }

    function displayLoadingEndError(){
        $('div#outputImageContainer div.btn').removeClass('disabled');
        $('#cardLoader').css('display', 'none');
    }

</script>
