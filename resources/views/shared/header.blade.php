<?php
$locale = App::getLocale();
$locales = config('app.locales');
?>
<nav>
    <div class="nav-wrapper">
        @if ( env('APP_ENV') !== 'production' )
            <a href="#" class="brand-logo center">⭐ {{ ucfirst(env('APP_ENV')) }}</a>
        @endif
        <ul id="nav-mobile" class="left">
            <?php
            $splitUri = explode('/', $_SERVER['REQUEST_URI']);
            ?>
            @foreach (['home'=>'','pilot cards'=>'pilots','upgrade cards'=>'upgrades','gallery'=>'gallery'] as $name => $route)
                <li
                    @if ((isset($splitUri[2]) && $splitUri[2] == $route) || (!isset($splitUri[2]) && $route == ''))
                    class="active"
                    @endif
                >
                    <a href="{{ url($locale.'/'.$route) }}">{{ ucfirst(__($name)) }}</a>
                </li>
            @endforeach
        </ul>
        <ul id="nav-mobile" class="right">
            <li>
                <div id="localeSelection" class="col m1 s2">
                    <a class='dropdown-trigger' href='#' data-target='dropdown1'>
                        <img class="localeFlag" src="/img/flags/{{ $locale }}.png"> {{ $locales[$locale] }}
                    </a>
                    <ul id='dropdown1' class='dropdown-content'>
                        @foreach ($locales as $key => $value)
                            <li><a href="/{{$key}}"><img class="localeFlag" src="/img/flags/{{$key}}.png"> {{ $value }}
                                </a></li>
                        @endforeach
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</nav>
