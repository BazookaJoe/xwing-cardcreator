<footer class="page-footer secondary">
    <div class="container">
        <div class="row">
            <div class="col m2 s8 offset-m1">
                <h5>{{ ucfirst(__('about')) }}</h5>
                <hr>
                <ul>
                    <li><a href="{{ url('en/about/') }}">{{ ucfirst(__('the website')) }}</a></li>
                    <li><a href="{{ url('en/about/api') }}">{{ __('API') }}</a></li>
                    <li><a href="{{ url('en/about/cookies') }}">{{ ucfirst(__('cookies')) }}</a></li>
                </ul>
            </div>
            <div class="col m2 s8">
                <h5>{{ ucfirst(__('contact')) }}</h5>
                <hr>
                <ul>
                    <li><i class="material-icons">mail</i>
                        <a href="mailto:{{ env('MAIL_CONTACT') }}">{{ env('MAIL_CONTACT') }}</a>
                    </li>
                    <li><a href="{{ env('DISCORD_INVIT') }}" target="_blank"><img src="{{asset('img/discord.png')}}">
                            Discord</a></li>
                </ul>
            </div>
            <div class="col m2 s8">
                <h5>{{ ucfirst(__('report issues')) }}</h5>
                <hr>
                <ul>
                    <li>
                        <a href="https://community.fantasyflightgames.com/topic/280657-20-upgrade-cards-creator/?page=4&tab=comments"
                           target="_blank">FFG Forum topic</a>
                    </li>
                    <li><a href="{{ env('GIT_REPO') }}"><img src="{{asset('img/gitlab.png')}}"> Gitlab repository</a></li>
                </ul>
            </div>
            <div class="col m4 s8">
                <h5>{{ ucfirst(__('changelog')) }}</h5>
                <hr>
                <ul>
                    @foreach(array_slice(App\Providers\GitlabApiProvider::getCommits(), 0, 5, true) as $commit)
                        <li>{{ date('d/m/Y', strtotime($commit['created_at'])). ' : ' .$commit['message'] }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container center">
            © Copyright 2019, Alexandre Moreau - All rights reserved.
        </div>
    </div>
</footer>
