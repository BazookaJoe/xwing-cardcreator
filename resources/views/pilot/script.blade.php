<script>
    var form = $("form#form-main");
    var cardgenOutput;
    var logOutput = true;
    var autoSubmit = false;
    var faction = 'empire';
    var ship = 'tiefighter';
    var stats;

    // TODO reparer les selects

    $(document).ready(function () {
        reloadSelect();

        $('.tabs').tabs();
        //$('div[id^=actionSelect], div[id^=red-actionSelect]').formSelect();
        setShip('tiefighter');
        reloadVisualisers();
        autoSubmit = true;
        form.submit();

    });

    $('div#cardPublishForm div.confirmSubmit').click(function (e) {
        publishCard();
        $('#cardPublishForm').hide();
    });

    $('div.btn.askSubmit').click(function () {
        console.log('e');
        if ($("#cardPublishForm").is(":visible")) {
            $('#cardPublishForm').hide();
        } else {
            $('#cardPublishForm').show();
        }
    });

    $('div.btn.create').click(function (e) {
        form.submit();
    });

    $('div.btn.reset').click(function (e) {
        form.trigger('reset');
        form.submit();
        reloadVisualisers();
    });

    form.change(function (e) {
        if (autoSubmit) {
            form.submit();
        }
    });

    form.on('reset', function () {
        faction = 'empire';
    });

    // ----------------------------------- Write symbols on click

    $('div#selectIcons div div').click(function (e) {

        console.log($(e.target).attr('symbolText'));
        let activeTextArea;

        if (M.Tabs.getInstance($('#ability-select')).index === 0) {
            activeTextArea = 'textarea[name=pilot-ability]';
        } else {
            activeTextArea = 'textarea[name=ship-ability]';
        }

        $(activeTextArea).val($(activeTextArea).val() + '!' + $(e.target).attr('symbolText') + ' ');
        // $('form#form-main').trigger('change');

    });


    // ----------------------------------- Action select initialisation

    // -> ici zone A

    // ----------------------------------- Faction select

    $('div#factionSelect div').click(function (e) {
        faction = $(e.target).attr('symbolText');

        M.Tabs.getInstance($('.tabs')[1]).select('ships' + faction.charAt(0).toUpperCase() + faction.slice(1));
        console.log('ships' + faction.charAt(0).toUpperCase() + faction.slice(1));
        form.trigger('change');
    });

    // ----------------------------------- Ship select

    $('div#shipSelect div div').click(function (e) {
        ship = $(e.target).attr('symbolText');
        setShip(ship);
    });

    function setShip() {
        $.ajax({
            url: '/api/v1/json/shipdata/' + ship,
            type: 'GET',
            success: function (stats) {
                try {

                    $('input[type=range][name=attack-front]').val(stats.attack_front);
                    $('input[type=range][name=attack-rear]').val(stats.attack_rear);
                    $('input[type=range][name=attack-singleMobile]').val(stats.attack_singleMobile);
                    $('input[type=range][name=attack-doubleMobile]').val(stats.attack_doubleMobile);
                    $('input[type=range][name=agility]').val(stats.agility);
                    $('input[type=range][name=hull]').val(stats.hull);
                    $('input[type=range][name=shield]').val(stats.shield);
                    $('textarea[name=ship-ability]').val(stats.shipAbility);

                    for (var i = 0; i < 5; i++) {
                        actionRow = stats.actions[i];
                        $('#actionSelect0' + (i + 1) + ' option').attr('selected', false);
                        $('#red-actionSelect0' + (i + 1) + ' option').attr('selected', false);
                        if (actionRow[0] != '') {
                            $('#actionSelect0' + (i + 1) + ' option[value="!' + actionRow[0] + '"]').attr('selected', true);
                        } else {
                            $('#actionSelect0' + (i + 1) + ' option[value=none]').attr('selected', true);
                        }
                        if (actionRow[1] != '') {
                            $('#red-actionSelect0' + (i + 1) + ' option[value="!' + actionRow[1] + '"]').attr('selected', true);
                        } else {
                            $('#red-actionSelect0' + (i + 1) + ' option[value=none]').attr('selected', true);
                        }
                    }

                    reloadSelect();
                    reloadVisualisers();
                    form.trigger('change');
                } catch (err) {
                    console.log('error while parsing');
                    console.log(err);
                    console.log(data);
                }

            }
        });
    }

    // ----------------------------------- Visualizers

    $('input[type=range]#limited').change(function (e) {
        var limited = $('input[type=range]#limited').val();
        if (limited > 3) {
            limited = 'no';
        }
        $('span#limited-visualizer').html(limited);
    });

    $('input[type=range][name=initiative]').change(function (e) {
        $('span#initiative-visualizer').html($('input[type=range][name=initiative]').val());
    });
    $('input[type=range][name=attack-front]').change(function (e) {
        $('span#attack-front-visualizer').html($('input[type=range][name=attack-front]').val());
    });
    $('input[type=range][name=attack-rear]').change(function (e) {
        $('span#attack-rear-visualizer').html($('input[type=range][name=attack-rear]').val());
    });
    $('input[type=range][name=attack-singleMobile]').change(function (e) {
        $('span#attack-singleMobile-visualizer').html($('input[type=range][name=attack-singleMobile]').val());
    });
    $('input[type=range][name=attack-doubleMobile]').change(function (e) {
        $('span#attack-doubleMobile-visualizer').html($('input[type=range][name=attack-doubleMobile]').val());
    });
    $('input[type=range][name=agility]').change(function (e) {
        $('span#agility-visualizer').html($('input[type=range][name=agility]').val());
    });
    $('input[type=range][name=hull]').change(function (e) {
        $('span#hull-visualizer').html($('input[type=range][name=hull]').val());
    });
    $('input[type=range][name=shield]').change(function (e) {
        $('span#shield-visualizer').html($('input[type=range][name=shield]').val());
    });
    $('input[type=range][name=force]').change(function (e) {
        $('span#force-visualizer').html($('input[type=range][name=force]').val());
    });
    $('input[type=range][name=charge]').change(function (e) {
        $('span#charge-visualizer').html($('input[type=range][name=charge]').val());
    });

    function reloadSelect() {
        $('select').formSelect();
        $('div[id^=actionSelect] li span, div[id^=actionSelect] input, div[id^=red-actionSelect] li span, div[id^=red-actionSelect] input').addClass('xwingFont');
        $('div[id^=actionSelect] li span, div[id^=actionSelect] input, div[id^=red-actionSelect] li span, div[id^=red-actionSelect] input').css('text-align', 'center');
        $('div[id^=red-actionSelect] input').css('color', 'red');
    }

    function reloadVisualisers() {
        $('span#limited-visualizer').html('no');
        autoSubmit = false;
        $('input[type=range][name=initiative]').trigger('change');
        $('input[type=range][name=attack-front]').trigger('change');
        $('input[type=range][name=attack-rear]').trigger('change');
        $('input[type=range][name=attack-singleMobile]').trigger('change');
        $('input[type=range][name=attack-doubleMobile]').trigger('change');
        $('input[type=range][name=agility]').trigger('change');
        $('input[type=range][name=hull]').trigger('change');
        $('input[type=range][name=shield]').trigger('change');
        $('input[type=range][name=force]').trigger('change');
        $('input[type=range][name=charge]').trigger('change');
        autoSubmit = true;
    }

    // ----------------------------------- Form submit

    form.submit(function (e) {
        e.preventDefault();

        var formData = new FormData();

        var cardName = '';
        var i = 0;

        if ($('form#form-main input#limited').val() < $('form#form-main input#limited').attr("max")) {
            while (i < $('form#form-main input#limited').val()) {
                cardName += '!lim ';
                i++;
            }
        }

        cardName += $('form#form-main input[name=card-name]').val();

        formData.append('_token', $('form#form-main input[name=_token]').val());

        formData.append('card-name', cardName);
        formData.append('pilot-title', $('form#form-main input[name=pilot-title]').val());

        formData.append('card-art-image', $('form#form-main input[type=file]')[0].files[0]);

        formData.append('pilot-ability', $('form#form-main textarea[name=pilot-ability]').val());
        formData.append('ship-ability', $('form#form-main textarea[name=ship-ability]').val());

        formData.append('card-art-image', $('form#form-main input[type=file]')[0].files[0]);

        formData.append('faction', faction);
        formData.append('ship', ship);

        formData.append('initiative', $('input[type=range][name=initiative]').val());
        formData.append('attack-front', $('input[type=range][name=attack-front]').val());
        formData.append('attack-rear', $('input[type=range][name=attack-rear]').val());
        formData.append('attack-singleMobile', $('input[type=range][name=attack-singleMobile]').val());
        formData.append('attack-doubleMobile', $('input[type=range][name=attack-doubleMobile]').val());

        formData.append('agility', $('input[type=range][name=agility]').val());
        formData.append('hull', $('input[type=range][name=hull]').val());
        formData.append('shield', $('input[type=range][name=shield]').val());
        formData.append('force', $('input[type=range][name=force]').val());
        formData.append('charge', $('input[type=range][name=charge]').val());

        var actions = '';
        var actionsRed = '';

        for (var i = 1; i <= 5; i++) {
            actions += $('div#actionSelect0' + i + ' select option:selected').val();
            actionsRed += $('div#red-actionSelect0' + i + ' select option:selected').val();
            if (i < 5) {
                actions += ',';
                actionsRed += ',';
            }
        }

        formData.append('actions', actions);
        formData.append('actions-red', actionsRed);

        console.log(formData);

        displayLoadingStart();

        $.ajax({
            url: '/pilots',
            method: 'POST',
            data: formData,
            success: function (output) {

                if (logOutput) {
                    console.log(output);
                }

                try {
                    if (output.success) {
                        displayLoadingEndSuccess();
                        let path = output.data.path + '?' + new Date().getTime();
                        $("div#outputImageContainer img").attr('src', path);
                        $("div#outputImageContainer a").attr('href', path);
                        $("div#outputImageContainer a").attr('download', $('form#form-main input[name=card-name]').val().replace(/ /g, "_"));

                    } else {
                        displayLoadingEndError();
                        M.toast({html: 'Something went wrong'});
                        console.log('fail');
                    }

                    // console.log('success');
                } catch (err) {
                    console.log('error while parsing');
                    console.log(output);
                }
            },
            failure: function (output) {
                console.log('failure');
                console.log(output);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    function publishCard() {

        let formData = new FormData();

        formData.append('_token', $('form#form-main input[name=_token]').val());

        formData.append('cardName', $('form#form-main input[name=card-name]').val());
        formData.append('username', $('form#form-main input[name=user-name]').val());

        $.ajax({
            url: '/publish',
            method: 'POST',
            data: formData,
            success: function (output) {

                if (logOutput) {
                    console.log(output);
                }

                console.log('Card saved to ' + 2);
                M.toast({html: 'Card published !'})

                try {

                    console.log(output);

                } catch (err) {
                    console.log('error while parsing');
                    console.log(output);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },

            cache: false,
            contentType: false,
            processData: false
        });
    }

    function logOutput() {
        logOutput = true;
    }
</script>
