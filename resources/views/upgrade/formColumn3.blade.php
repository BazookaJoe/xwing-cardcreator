<div class="col s12 l4">
    <div>
        <label for="grant-attack">Grant attack</label>
        <div class="switch">
            <label>No<input type="checkbox" name="grant-attack"><span class="lever"></span>Yes</label>
        </div>
        <div id="secondary-weapon-stats">
            <div class="inputAttackColor">
                <label for="require">Require</label>
                <div class="inputContainer">
                    <label><input type="radio" name="require" value="" checked><span>nothing</span></label>
                    <label><input type="radio" name="require" value="!tlk"><span>target lock</span></label>
                    <label><input type="radio" name="require" value="!foc"><span>focus</span></label>
                </div>
                <label for="arc-type">Firing arc type</label>
                <div class="inputContainer">
                    <label><input type="radio" name="arc-type" value="classic" checked><span><i
                                class="xwing-miniatures-font xwing-miniatures-font-frontarc"></i></span></label>
                    <label><input type="radio" name="arc-type" value="rear-classic"><span><i
                                class="xwing-miniatures-font xwing-miniatures-font-reararc"></i></span></label>
                    <label><input type="radio" name="arc-type" value="bullseye"><span><i
                                class="xwing-miniatures-font xwing-miniatures-font-bullseyearc"></i></span></label>
                    <label><input type="radio" name="arc-type" value="rear-bullseye"><span><i
                                class="xwing-miniatures-font xwing-miniatures-font-rearbullseyearc"></i></span></label>
                    <label><input type="radio" name="arc-type" value="single-rotating"><span><i
                                class="xwing-miniatures-font xwing-miniatures-font-singleturretarc"></i></span></label>
                    <label><input type="radio" name="arc-type" value="double-rotating"><span><i
                                class="xwing-miniatures-font xwing-miniatures-font-doubleturretarc"></i></span></label>
                </div>
                <br/>
                <div class="row">
                    <div class="col s12 m6">
                        <label for="dice-number">Number of dices <span id="dice-number-visualizer" class="badge"
                                                                       title="Number of dices"></span></label>
                        <input type="range" id="dice-number" name="min-range" min="1" max="9" value="3"/>
                    </div>
                    <div class="col s12 m6">
                        <label for="range-bonus">Range bonus</label>
                        <div class="switch">
                            <label>No<input type="checkbox" name="range-bonus"><span class="lever"></span>Yes</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 m6">
                        <label for="min-range">Minimum range <span id="min-range-visualizer" class="badge"
                                                                   title="Minimum range"></span></label>
                        <input type="range" id="min-range" name="min-range" min="0" max="5" value="1"/>
                    </div>
                    <div class="col s12 m6">
                        <label for="max-range">Maximum range <span id="max-range-visualizer" class="badge"
                                                                   title="Maximum range"></span></label>
                        <input type="range" id="max-range" name="max-range" min="0" max="5" value="3"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br/>

    <label for="grant-action">Grant action</label>
    <div class="input-field">

        <div class="inputContainer">
            <label><input type="checkbox" name="grant-action" value="red"
                          class="filled-in"><span>red</span></label><br/>
            <div class="row">
                @include('upgrade.utils.grantAction', ['value' => 'foc', 'name'=>'focus'])
                @include('upgrade.utils.grantAction', ['value' => 'cal', 'name'=>'calculate'])
                @include('upgrade.utils.grantAction', ['value' => 'tlk', 'name'=>'lock'])
                @include('upgrade.utils.grantAction', ['value' => 'boo', 'name'=>'boost'])
                @include('upgrade.utils.grantAction', ['value' => 'eva', 'name'=>'evade'])
                @include('upgrade.utils.grantAction', ['value' => 'bar', 'name'=>'barrelroll'])
                @include('upgrade.utils.grantAction', ['value' => 'rei', 'name'=>'reinforce'])
                @include('upgrade.utils.grantAction', ['value' => 'rel', 'name'=>'reload'])
                @include('upgrade.utils.grantAction', ['value' => 'rot', 'name'=>'rotatearc'])
                @include('upgrade.utils.grantAction', ['value' => 'jam', 'name'=>'jam'])
                @include('upgrade.utils.grantAction', ['value' => 'clk', 'name'=>'cloak'])
                @include('upgrade.utils.grantAction', ['value' => 'coo', 'name'=>'coordinate'])
                @include('upgrade.utils.grantAction', ['value' => 'sla', 'name'=>'slam'])
                <div class="col s2 m3">
                    <label>
                        <input value="none" name="grant-action" type="radio" checked/>
                        <span>none</span>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <br/>


    <div class="row">

    </div>

    <div class="row">
        <div class="col s12" style="padding-bottom: 1em">
            <label for="hull-bonus_tab">Bonuses</label>
            <ul class="tabs" id="bonus_tab">
                <li class="tab col s3"><a class="active" href="#tab_charge">
                    Charges
                </a></li>
                <li class="tab col s3"><a href="#tab_hullShield">
                    Hull/Shield
                </a></li>
                <li class="tab col s3"><a href="#tab_force">Force</a></li>
            </ul>
        </div>
        <div id="tab_charge" class="row col s12">
            <div class="col s8">
                <label for="charge-number">Charges <span id="charge-number-visualizer" class="badge" title="Charges"></span></label>
                <input type="range" id="charge-number" name="charge-number" min="0" max="6" value="0"/>
            </div>
            <div class="col s4">
                <label for="charge-regenerate">Regenerate</label>
                <div class="switch">
                    <label>No<input type="checkbox" name="charge-regenerate"><span class="lever"></span>Yes</label>
                </div>
            </div>
        </div>
        <div id="tab_hullShield" class="row col s12" >
            <div class="col s6">
                <label for="hull-number">
                    Hull <span id="hull-number-visualizer" class="badge" title="Charges"></span>
                </label>
                <input type="range" name="hull-number" min="-2" max="6" value="0"/>
            </div>
            <div class="col s6">
                <label for="shield-number">
                    Shield <span id="shield-number-visualizer" class="badge" title="Shield"></span>
                </label>
                <input type="range" name="shield-number" min="-2" max="6" value="0"/>
            </div>
        </div>
        <div id="tab_force" class="row col s12">
            <div class="col s8">
                <label for="force-number">Force <span id="force-number-visualizer" class="badge" title="Charges"></span></label>
                <input type="range" id="force-number" name="force-number" min="0" max="6" value="0"/>
            </div>
            <div class="col s4">
                <label for="force-regenerate">Regenerate</label>
                <div class="switch">
                    <label>No<input type="checkbox" name="force-regenerate"><span class="lever"></span>Yes</label>
                </div>
            </div>
        </div>
    </div>

    <div>
        <label for="card-text">Restriction text</label>
        <textarea rows="4" cols="50" name="restrictions-text"></textarea>
        <div id="restrictionsInput">
            <label for="restrictions-faction">Faction</label>
            <div class="inputContainer">
                <label><input type="checkbox" name="restrictions-faction" value="imperial" class="filled-in"><span><i
                            class="xwing-miniatures-font xwing-miniatures-font-empire"></i></span></label>
                <label><input type="checkbox" name="restrictions-faction" value="rebel" class="filled-in"><span><i
                            class="xwing-miniatures-font xwing-miniatures-font-rebel"></i></span></label></i>
                <label><input type="checkbox" name="restrictions-faction" value="scum" class="filled-in"><span><i
                            class="xwing-miniatures-font xwing-miniatures-font-scum"></i></span></label>
                <label><input type="checkbox" name="restrictions-faction" value="first order" class="filled-in"><span><i
                            class="xwing-miniatures-font xwing-miniatures-font-firstorder"></i></span></label>
                <label><input type="checkbox" name="restrictions-faction" value="resistance" class="filled-in"><span><i
                            class="xwing-miniatures-font xwing-miniatures-font-rebel-outline"></i></span></label>
            </div>
            <label for="restrictions-ship-size">Ship size</label>
            <div class="inputContainer">
                <label><input type="checkbox" name="restrictions-ship-size" value="small"
                              class="filled-in"><span>small</span></label>
                <label><input type="checkbox" name="restrictions-ship-size" value="medium" class="filled-in"><span>medium</span></label>
                <label><input type="checkbox" name="restrictions-ship-size" value="large"
                              class="filled-in"><span>large</span></label>
                <label><input type="checkbox" name="restrictions-ship-size" value="huge"
                              class="filled-in"><span>huge</span></label>
            </div>
            <label for="restrictions-action">Actions</label>
            <div class="inputContainer">
                <label><input type="checkbox" name="restrictions-action" value="RED" class="filled-in"><span>red</span></label><br/>

                <div class="row">
                    <div class="col s2 m3">
                        <label>
                            <input value="foc" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-focus"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="cal" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-calculate"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="tlk" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-lock"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="boo" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-boost"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="eva" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-evade"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="bar" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-barrelroll"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="rei" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-reinforce"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="rel" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-reload"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="rot" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-rotatearc"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="jam" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-jam"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="clk" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-cloak"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="coo" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-coordinate"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="sla" name="restrictions-action" type="radio"/>
                            <span><i class="xwing-miniatures-font xwing-miniatures-font-slam"></i></span>
                        </label>
                    </div>
                    <div class="col s2 m3">
                        <label>
                            <input value="none" name="restrictions-action" type="radio" checked/>
                            <span>none</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
