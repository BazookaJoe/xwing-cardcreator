<script>
    var form = $("form#form-main");
    var cardgenOutput;
    var logOutput = false;
    var autoSubmit = false;
    var selectedcardType = 'ept';

    $(document).ready(function () {

        // Materialiez loaders
        // $('.tooltipped').tooltip();

        //$('select').formSelect();
        reloadVisualisers();
        autoSubmit = true;
        formSubmit();

    });

    $('div#cardPublishForm div.confirmSubmit').click(function (e) {
        publishCard();
        $('#cardPublishForm').hide();
    });

    $('div.btn.askSubmit').click(function (e) {
        if ($("#cardPublishForm").is(":visible")) {
            $('#cardPublishForm').hide();
        } else {
            $('#cardPublishForm').show();
        }
    });

    $('div.btn.create').click(function (e) {
        formSubmit();
    });

    $('div.btn.reset').click(function (e) {
        form.trigger('reset');
        formSubmit();
        reloadVisualisers();
    });

    form.change(function (e) {
        if (autoSubmit) {
            formSubmit();
        }
    });

    form.on('reset', function () {
        selectedcardType = 'ept';
    });

    // ----------------------------------- Input utofocus

    //$('img').click(function(){$('textarea[name=card-text]').focus();});


    // ----------------------------------- Write symbols on click

    $('div#selectIcons div div').click(function (e) {

        $('textarea[name=card-text]').val($('textarea[name=card-text]').val() + '!' + $(e.target).attr('symbolText') + ' ');
        $('form#form-main').trigger('change');

    });


    // ----------------------------------- Enable or disable secondary weapons div

    $('div#selectCardType div div').click(function (e) {
        selectedcardType = $(e.target).attr('symbolText');

        if (selectedcardType == 'torpedoes' || selectedcardType == 'missiles' || selectedcardType == 'cannon' || selectedcardType == 'turret') {
            $('input[type=checkbox][name=grant-attack]').prop('checked', true);
            // Erase text if ^ changed
        } else {
            $('input[type=checkbox][name=grant-attack]').prop('checked', false);
            // Erase text if ^ changed
        }

        form.trigger('change');

    });


    $('input[name=require], input[type=checkbox][name=grant-attack]').change(function (e) {

        var grantAttack = $('input[type=checkbox][name=grant-attack]').prop('checked');
        var attackRequire = $('input[name=require]:checked').val();
        if (grantAttack) {
            $('form#form-main textarea[name=card-text]').val('**ATTACK [ ' + attackRequire + ' ]: **');
        } else {
            $('form#form-main textarea[name=card-text]').val('');
        }
    });

    // ----------------------------------- Create attack text

    $('select[name=card-type], form#form-main input[name=require], form#form-main input[type=range]#charge-number').change(function (e) {
        var cardType = $('select[name=card-type] option:selected').val();
        if (cardType == 'torpedoes' || cardType == 'missiles' || cardType == 'cannon' || cardType == 'turret') {
            var oldText = $('textarea[name=card-text]').html();
            var text = '';
            var require = $('form#form-main input[name=require]:checked').val();
            var charges = $('form#form-main input[type=range]#charge-number').val()

            text += 'Attack';
            if (require != 'nothing') {
                if (require == 'target-lock') {
                    text += ' (!tlk)';
                } else {
                    text += ' (!foc)';
                }
            }
            text += ': ';

            if (charges != '0') {
                text += 'Spend 1 !cha'
            }

//$('textarea[name=card-text]').val(text);
        }
    });

    // ----------------------------------- Create restrictions text

    $('div#restrictionsInput div label input').change(function () {
        restrictionText = '';

        if ($('div#restrictionsInput input[name=restrictions-action]:checked').length > 0 && $('div#restrictionsInput input[name=restrictions-action][value=none]').prop('checked') != true) {

            $('div#restrictionsInput input[name=restrictions-action]:checked').each(function (i, e) {
                if (e.value == 'RED') {
                    restrictionText += e.value + ' ';
                } else {
                    restrictionText += '!' + e.value + ' ';
                }
            });

        } else {

            $('div#restrictionsInput input[name=restrictions-faction]:checked').each(function (i, e) {
// Last element
                if (i == $('div#restrictionsInput input[name=restrictions-faction]:checked').length - 1) {
// If there are restriction on ship size
                    if ($('div#restrictionsInput input[name=restrictions-ship-size]:checked').length > 0) {
                        restrictionText += e.value.toUpperCase() + ', ';
                    } else {
                        restrictionText += e.value.toUpperCase();
                    }
// Not last element
                } else {
                    restrictionText += e.value.toUpperCase() + ' OR ';
                }
            });

            $('div#restrictionsInput input[name=restrictions-ship-size]:checked').each(function (i, e) {
                if (i == $('div#restrictionsInput input[name=restrictions-ship-size]:checked').length - 1) {
                    restrictionText += e.value.toUpperCase() + ' SHIP';
                } else {
                    restrictionText += e.value.toUpperCase() + ' OR ';
                }
            });

        }

        $('form#form-main textarea[name=restrictions-text]').val(restrictionText);

    });

    // ----------------------------------- Visualisers

    $('input[type=range]#limited').change(function (e) {
        var limited = $('input[type=range]#limited').val();
        if (limited > 3) {
            limited = 'no';
        }
        $('span#limited-visualizer').html(limited);
    });

    $('input[type=range]#dice-number').change(function (e) {
        $('span#dice-number-visualizer').html($('input[type=range]#dice-number').val());
    });

    $('input[type=range]#min-range').change(function (e) {
        $('span#min-range-visualizer').html($('input[type=range]#min-range').val());
        if ($('input[type=range]#min-range').val() > $('input[type=range]#max-range').val()) {
            $('input[type=range]#max-range').val($('input[type=range]#min-range').val());
            $('input[type=range]#max-range').trigger('change');
        }
    });

    $('input[type=range]#max-range').change(function (e) {
        $('span#max-range-visualizer').html($('input[type=range]#max-range').val());
        if ($('input[type=range]#min-range').val() > $('input[type=range]#max-range').val()) {
            $('input[type=range]#min-range').val($('input[type=range]#max-range').val());
            $('input[type=range]#min-range').trigger('change');
        }
    });

    $('input[type=range]#charge-number').change(function (e) {
        $('span#charge-number-visualizer').html($('input[type=range]#charge-number').val());
    });

    $('input[type=range]#force-number').change(function (e) {
        $('span#force-number-visualizer').html($('input[type=range]#force-number').val());
    });

    $('input[type=range][name=hull-number]').change(function (e) {
        $('span#hull-number-visualizer').html($('input[type=range][name=hull-number]').val());
    });

    $('input[type=range][name=shield-number]').change(function (e) {
        $('span#shield-number-visualizer').html($('input[type=range][name=shield-number]').val());
    });

    function reloadVisualisers() {
        $('span#limited-visualizer').html('no');
        $('span#dice-number-visualizer').html($('input[type=range]#dice-number').val());
        $('span#min-range-visualizer').html($('input[type=range]#min-range').val());
        if ($('input[type=range]#min-range').val() > $('input[type=range]#max-range').val()) {
            $('input[type=range]#max-range').val($('input[type=range]#min-range').val());
            $('input[type=range]#max-range').trigger('change');
        }
        $('span#max-range-visualizer').html($('input[type=range]#max-range').val());
        if ($('input[type=range]#min-range').val() > $('input[type=range]#max-range').val()) {
            $('input[type=range]#min-range').val($('input[type=range]#max-range').val());
            $('input[type=range]#min-range').trigger('change');
        }
        $('span#charge-number-visualizer').html($('input[type=range]#charge-number').val());
        $('span#force-number-visualizer').html($('input[type=range]#force-number').val());
        $('span#hull-number-visualizer').html($('input[type=range][name=hull-number]').val());
        $('span#shield-number-visualizer').html($('input[type=range][name=shield-number]').val());
    }

    // ----------------------------------- Form submit

    function formSubmit() {

        var formData = new FormData();

        var cardName = '';
        var i = 0;

        if ($('form#form-main input#limited').val() < $('form#form-main input#limited').attr("max")) {
            while (i < $('form#form-main input#limited').val()) {
                cardName += '!lim ';
                i++;
            }
        }

        cardName += $('form#form-main input[name=card-name]').val();

        formData.append('_token', $('form#form-main input[name=_token]').val());
        formData.append('userName', $('form#form-main input[name=user-name]').val())

        formData.append('card-name', cardName);

        formData.append('card-art-image', $('form#form-main input[type=file]')[0].files[0]);

        formData.append('card-type', selectedcardType);

        formData.append('grant-attack', $('form#form-main input[type=checkbox][name=grant-attack]').prop('checked'));

        formData.append('grant-action', $('form#form-main input[type=radio][name=grant-action]:checked').val());

        formData.append('grant-action-red', $('form#form-main input[type=checkbox][name=grant-action]').prop('checked'));

        formData.append('arc-type', $('form#form-main input[name=arc-type]:checked').val());
        formData.append('dice-number', $('form#form-main input#dice-number').val());
        formData.append('range-bonus', $('form#form-main input[type=checkbox][name=range-bonus]').prop('checked'));
        formData.append('min-range', $('form#form-main input#min-range').val());
        formData.append('max-range', $('form#form-main input#max-range').val());

        formData.append('charge-number', $('form#form-main input[type=range]#charge-number').val());
        formData.append('charge-regenerate', $('input[type=checkbox][name=charge-regenerate]').prop('checked'));
        formData.append('force-number', $('form#form-main input[type=range]#force-number').val());
        formData.append('force-regenerate', $('input[type=checkbox][name=force-regenerate]').prop('checked'));
        formData.append('hull-number', $('form#form-main input[type=range][name=hull-number]').val());
        formData.append('shield-number', $('form#form-main input[type=range][name=shield-number]').val());

        formData.append('card-text', $('form#form-main textarea[name=card-text]').val());

        formData.append('restrictions-text', $('form#form-main textarea[name=restrictions-text]').val());

        displayLoadingStart();

        $.ajax({
            url: '/upgrades',
            method: 'POST',
            data: formData,
            success: function (output) {

                if (logOutput) {
                    console.log(output);
                }

                try {

                    // jQuery.each(jData.log, function (i, val) {
                    //     console.log(i + ' : ' + val);
                    // });
                    //
                    // jQuery.each(jData.errors, function (i, val) {
                    //     console.log('Error ' + i + ' : ' + val);
                    // });

                    if (output.success) {
                        displayLoadingEndSuccess();
                        let path = output.data.path + '?' + new Date().getTime();
                        $("div#outputImageContainer img").attr('src', path);
                        $("div#outputImageContainer a").attr('href', path);
                        $("div#outputImageContainer a").attr('download', $('form#form-main input[name=card-name]').val().replace(/ /g, "_"));

                    } else {
                        displayLoadingEndError();
                        M.toast({html: 'Something went wrong'});
                        console.log('fail');
                    }
                    // console.log('success');
                } catch (err) {
                    console.log('error while parsing');
                    console.log(data);
                }
            },
            failure: function (output) {
                console.log('failure');
                console.log(output);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function publishCard() {

        let formData = new FormData();

        formData.append('_token', $('form#form-main input[name=_token]').val());

        formData.append('cardName', $('form#form-main input[name=card-name]').val());
        formData.append('username', $('form#form-main input[name=user-name]').val());

        $.ajax({
            url: '/publish',
            method: 'POST',
            data: formData,
            success: function (output) {

                if (logOutput) {
                    console.log(output);
                }

                console.log('Card saved to ' + 2);
                M.toast({html: 'Card published !'})

                try {

                    console.log(output);

                } catch (err) {
                    console.log('error while parsing');
                    console.log(output);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },

            cache: false,
            contentType: false,
            processData: false
        });
    }
</script>
