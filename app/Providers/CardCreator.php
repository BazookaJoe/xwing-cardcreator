<?php

namespace App\Providers;

interface CardCreator
{
    public static function processFiles(array $files): array;

    public static function processParams(array $params): array;

    public static function createCard(array $params, array $files, string $id, bool $privatePath = false);
}
