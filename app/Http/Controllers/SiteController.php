<?php

namespace App\Http\Controllers;

use App\Providers\CardCreatorProvider;
use App\Providers\DiscordMessageProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class SiteController extends Controller
{
    public function index()
    {
        $randomGalleryCards = [];
        $galleryCards = CardCreatorProvider::getPublishedCardsPath();
        switch (sizeof($galleryCards)) {
            case 0:
                break;
            case 1:
                array_push($randomGalleryCards, $galleryCards[0]);
                break;
            default:
                $numberOfCards = sizeof($galleryCards) >= 4 ? 4 : sizeof($galleryCards);
                $indexes = array_rand($galleryCards, $numberOfCards);
                foreach ($indexes as $index) {
                    array_push($randomGalleryCards, $galleryCards[$index]);
                }
                break;
        }
        return view('index', ['gallery_cards' => $randomGalleryCards]);
    }

    public function wip()
    {
        return view('wip');
    }

    public function gallery()
    {
        return view('gallery.index', ['cards' => CardCreatorProvider::getPublishedCardsPath()]);
    }

    public function publishCard()
    {
        $path = str_replace(' ', '%20', CardCreatorProvider::publishCard($_POST['cardName'], $_POST['username'], Session::getId()));
        DiscordMessageProvider::sendMessage("New card published by " . $_POST['username'] . ' : ' . $path, env('DISCORD_HOOK'));
        $output = ['path' => $path];
        // set session $_post[username]
        return response()->json($output);
    }

    public function about()
    {
        return view('about.index');
    }

    public function aboutCookies()
    {
        return view('about.cookies');
    }
}
